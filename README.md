The program that attempts to optimize the given pdf. This utility uses Ghostscript and qpdf, and can reduce pdf to ~35% of their size.

Dependencies:
* Ghostscript
* QPDF

Usage:
1. `chmod +x optpdf.sh`
2. `./optpdf.sh` *path/to/pdf*
   
OR

1. `make` (or `make build_cc` for C++ version)
2. `./optpdf` *path/to/pdf*
