/*
 * optpdf
 * The programm that attempts to optimize the given pdf.
 * Copyright 2020, Darkness9724
 * Licensed under GNU General Public License (GPL) version 3.
 * https://www.gnu.org/licenses/gpl-3.0.html
 * Redistributions of files must retain the above copyright notice.
*/

#include <iapi.h>
#include <ierrors.h>
#include <fcntl.h>
#include <glob.h>
#include <iostream>
#include <QPDF.hh>
#include <QPDFWriter.hh>
#include <cstring>
#include <sstream>

void* minst = NULL;

void display_help() {
    std::cout << "Usage: optpdf [filename or OPTIONS]\n\
            -h, --help    show this help and exit" << std::endl;
}

const std::string pdf_version(char* filename) {
    QPDF qpdf;
    qpdf.processFile(filename);

    return qpdf.getPDFVersion();
}

int optpdf(char* filename) {
    int code, code1;
    const char* gsargv[11];
    int gsargc;
    std::string optfile = filename;
    optfile.erase(strlen(filename)-strlen(".pdf"), strlen(filename));
    std::stringstream ss;
    ss << optfile << "_opt.pdf";
    optfile = ss.str();
    ss.str(std::string());
    ss << "-sOutputFile=" << optfile;
    std::string outfile = ss.str();
    ss.str(std::string());
    ss << "-dCompatibilityLevel=" << pdf_version(filename);
    std::string version = ss.str();
    ss.str(std::string());
    gsargv[0] = "";
    gsargv[1] = "-sDEVICE=pdfwrite";
    gsargv[2] = version.c_str();
    gsargv[3] = "-dPDFSETTINGS=/ebook";
    gsargv[4] = "-dNOPAUSE";
    gsargv[5] = "-dQUIET";
    gsargv[6] = "-dBATCH";
    gsargv[7] = "-dCompressFonts=true";
    gsargv[8] = "-dDetectDuplicateImages=true";
    gsargv[9] = outfile.c_str();
    gsargv[10] = filename;
    gsargc=11;

    if (gsapi_new_instance(&minst, NULL) < 0) {
        return 1;
    }
    if (gsapi_set_arg_encoding(minst, GS_ARG_ENCODING_UTF8) == 0) {
        code = gsapi_init_with_args(minst, gsargc, (char**)gsargv);
    } else {
        code = 0;
    }
    
    code1 = gsapi_exit(minst);
    if ((code == 0) || (code == gs_error_Quit))
        code = code1;

    gsapi_delete_instance(minst);
    if ((code == 0) || (code == gs_error_Quit))
        return 0;
    return 1;
}

int linearize(char* filename) {
    QPDF qpdf;

    std::string optfile = filename;
    optfile.erase(strlen(filename)-strlen(".pdf"), strlen(filename));
    std::stringstream ss;
    ss << optfile << "_opt.pdf";
    optfile = ss.str();
    ss.str(std::string());
    ss << filename << ".tmp";
    std::string tmp = ss.str();
    ss.str(std::string());

    qpdf.processFile(optfile.c_str());

    QPDFWriter output(qpdf, tmp.c_str());
    output.setLinearization(true);
    output.setCompressStreams(true);
    output.setStreamDataMode(qpdf_s_compress);
    output.setObjectStreamMode(qpdf_o_generate);
    output.setRecompressFlate(true);
    output.write();

    rename(tmp.c_str(), optfile.c_str());
    return 0;
}

int fsize(FILE *file){
    int prev = ftell(file);
    fseek(file, 0L, SEEK_END);
    int size = ftell(file);
    fseek(file, prev, SEEK_SET);
    
    return size;
}

int main(int argc, char** argv) {
    if (argc == 1 || argc > 2)
        display_help();
    else {
        for (int i = 1; i < argc; ++i) {
            if ((*argv[i]) == '-') {
                const char* p = argv[i]+1;

                while ((*p) != '\0') {
                    char c = *(p++);
                    if ((c == 'h') || (c == 'H'))
                        display_help();
                }
            } else {
                if(optpdf(argv[1])) {
                    return 1;
                } else {
                    if(linearize(argv[1])) return 1;
                    glob_t globbuf;
                    glob("*_opt.pdf", 0, NULL, &globbuf);
                    if (globbuf.gl_pathc) {
                        int orgsize = fsize(fopen(argv[1], "rb"));
                        int optsize = fsize(fopen(globbuf.gl_pathv[0], "rb"));
                        if (optsize == 0) {
                            std::cout << "No output! Keeping original." << std::endl;
                            remove(globbuf.gl_pathv[0]);
                            return 1;
                        } else if (optsize == orgsize) {
                            std::cout << "Didn't make it smaller! Keeping original." << std::endl;
                            remove(globbuf.gl_pathv[0]);
                            return 1;
                        }
                        std::cout << "Saving " << orgsize - optsize <<
                        " bytes (compression ratio " << 100 - (float)optsize * 100 / (float)orgsize <<
                        "%)" << std::endl;
                    } else return 1;
                }
            }
        }
    }
    return 0;
}
