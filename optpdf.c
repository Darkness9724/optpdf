/*
 * optpdf
 * The programm that attempts to optimize the given pdf.
 * Copyright 2020, Darkness9724
 * Licensed under GNU General Public License (GPL) version 3.
 * https://www.gnu.org/licenses/gpl-3.0.html
 * Redistributions of files must retain the above copyright notice.
*/

#include <iapi.h>
#include <ierrors.h>
#include <fcntl.h>
#include <glob.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <qpdf-c.h>

void* minst = NULL;

void display_help() {
    printf("Usage: optpdf [filename or OPTIONS]\n\
            -h, --help    show this help and exit\n");
}

const char* pdf_version(char* filename) {
    qpdf_data qpdf = qpdf_init();
    if ((qpdf_read(qpdf, filename, NULL) & QPDF_ERRORS))
        return NULL;
    const char* tmp = qpdf_get_pdf_version(qpdf);
    qpdf_cleanup(&qpdf);
    
    return tmp;
}

int optpdf(char* filename) {
    int code, code1;
    char* gsargv[11];
    int gsargc;
    char* optfile = (char*)malloc(1+strlen("-sOutputFile=")+strlen(filename));
    char* version = (char*)malloc(1+strlen("-dCompatibilityLevel=")+strlen(pdf_version(filename)));
    gsargv[0] = "";
    gsargv[1] = "-sDEVICE=pdfwrite";
    gsargv[2] = strcat(strcpy(version, "-dCompatibilityLevel="), pdf_version(filename));
    gsargv[3] = "-dPDFSETTINGS=/ebook";
    gsargv[4] = "-dNOPAUSE";
    gsargv[5] = "-dQUIET";
    gsargv[6] = "-dBATCH";
    gsargv[7] = "-dCompressFonts=true";
    gsargv[8] = "-dDetectDuplicateImages=true";
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wstringop-overflow"
    gsargv[9] = strcat(strncat(strcpy(optfile, "-sOutputFile="), filename,
                                strlen(filename)-strlen(".pdf")), "_opt.pdf");
    #pragma GCC diagnostic pop
    gsargv[10] = filename;
    gsargc=11;

    if (gsapi_new_instance(&minst, NULL) < 0) {
        free(optfile);
        free(version);
        return 1;
    }
    if (gsapi_set_arg_encoding(minst, GS_ARG_ENCODING_UTF8) == 0) {
        code = gsapi_init_with_args(minst, gsargc, gsargv);
    } else {
        code = 0;
    }
    
    code1 = gsapi_exit(minst);
    if ((code == 0) || (code == gs_error_Quit))
        code = code1;

    gsapi_delete_instance(minst);
    free(optfile);
    free(version);
    if ((code == 0) || (code == gs_error_Quit))
        return 0;
    return 1;
}

int linearize(char* filename) {
    qpdf_data qpdf = qpdf_init();
    int warnings = 0;
    int errors = 0;

    char* optfile = (char*)malloc(1+strlen(filename));
    #pragma GCC diagnostic push
    #pragma GCC diagnostic ignored "-Wstringop-overflow"
    optfile = strcat(strncpy(optfile, filename,
                                strlen(filename)-strlen(".pdf")), "_opt.pdf");
    #pragma GCC diagnostic pop
    char* tmp = (char*)malloc(1+strlen(".tmp")+strlen(optfile));
    tmp = strcat(strcpy(tmp, optfile), ".tmp");

    if (((qpdf_read(qpdf, optfile, NULL) & QPDF_ERRORS) == 0) &&
    ((qpdf_init_write(qpdf, tmp) & QPDF_ERRORS) == 0))
    {
        qpdf_set_static_ID(qpdf, QPDF_TRUE); /* for testing only */
        qpdf_set_linearization(qpdf, QPDF_TRUE);
        qpdf_set_compress_streams(qpdf, QPDF_TRUE);
        qpdf_set_stream_data_mode(qpdf, qpdf_s_compress);
        qpdf_set_object_stream_mode(qpdf, qpdf_o_generate);
        qpdf_write(qpdf);
    }
    while (qpdf_more_warnings(qpdf)) {
        warnings = 1;
        printf("warning: %s\n",
            qpdf_get_error_full_text(qpdf, qpdf_next_warning(qpdf)));
    }
    if (qpdf_has_error(qpdf)) {
        errors = 1;
        printf("error: %s\n",
            qpdf_get_error_full_text(qpdf, qpdf_get_error(qpdf)));
    }
    qpdf_cleanup(&qpdf);
    if(errors) {
        free(optfile);
        free(tmp);
        remove(tmp);
        return 2;
    }
    else if (warnings) {
        free(optfile);
        free(tmp);
        remove(tmp);
        return 3;
    }
    rename(tmp, optfile);
    free(optfile);
    free(tmp);
    return 0;
}

int fsize(FILE *file){
    int prev = ftell(file);
    fseek(file, 0L, SEEK_END);
    int size = ftell(file);
    fseek(file, prev, SEEK_SET);
    
    return size;
}

int main(int argc, char** argv) {
    if (argc == 1 || argc > 2)
        display_help();
    else {
        for (int i = 1; i < argc; ++i) {
            if ((*argv[i]) == '-') {
                const char* p = argv[i]+1;

                while ((*p) != '\0') {
                    char c = *(p++);
                    if ((c == 'h') || (c == 'H'))
                        display_help();
                }
            } else {
                if(optpdf(argv[1])) {
                    return 1;
                } else {
                    if(linearize(argv[1])) return 1;
                    glob_t globbuf;
                    glob("*_opt.pdf", 0, NULL, &globbuf);
                    if (globbuf.gl_pathc) {
                        int orgsize = fsize(fopen(argv[1], "rb"));
                        int optsize = fsize(fopen(globbuf.gl_pathv[0], "rb"));
                        if (optsize == 0) {
                            printf("No output! Keeping original.\n");
                            remove(globbuf.gl_pathv[0]);
                            return 1;
                        } else if (optsize == orgsize) {
                            printf("Didn't make it smaller! Keeping original.\n");
                            remove(globbuf.gl_pathv[0]);
                            return 1;
                        }
                        printf("Saving %d bytes (compression ratio %.2f%%)\n",
                               orgsize - optsize,
                               100 - (float)optsize * 100 / (float)orgsize);
                    } else return 1;
                }
            }
        }
    }
    return 0;
}
