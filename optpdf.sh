#!/bin/sh

# optpdf
# The script that attempts to optimize the given pdf.
# Copyright 2020, Darkness9724
# Licensed under GNU General Public License (GPL) version 3.
# https://www.gnu.org/licenses/gpl-3.0.html
# Redistributions of source code must retain the above copyright notice.
        
display_help() {
    echo "Usage: $(basename "$0") [filename or OPTIONS]" >&2
    echo "-h, --help    show this help and exit."
}

optpdf() {
    file=$1
    filebase="$(basename "$file" .pdf)"
    optfile="${filebase}_opt.pdf"
    version=$(pdfinfo "$file" | grep -oP "PDF version:    \K[^\b]+")
    gs -sDEVICE=pdfwrite -dCompatibilityLevel="$version" -dPDFSETTINGS=/ebook -dNOPAUSE -dQUIET -dBATCH \
            -dCompressFonts=true -dDetectDuplicateImages=true \
            -sOutputFile="${optfile}" "${file}"
    qpdf --object-streams=generate --compress-streams=y --stream-data=compress --compression-level=9 --recompress-flate --optimize-images --linearize --replace-input "${optfile}"
    if [ $? -eq 0 ]; then
        optsize=$(stat -c "%s" "${optfile}")
        orgsize=$(stat -c "%s" "${file}")
        if [ "${optsize}" -eq 0 ]; then
            echo "No output! Keeping original."
            rm -f "${optfile}"
            exit;
        fi
        if [ "${optsize}" -ge "${orgsize}" ]; then
           echo "Didn't make it smaller! Keeping original."
           rm -f "${optfile}"
           exit;
        fi
        bytesSaved=$(("$orgsize" - "$optsize"))
        percent=$((100 - "$optsize" * 100 / "$orgsize"))
        echo "Saving ${bytesSaved} bytes (compression ratio ${percent}%)"
    fi
}

for i in "$@"
do
    case "$i" in
        -h | --help)
            display_help
            exit 0
            ;;
        -*)
            echo "Error: Unknown option: $i" >&2
            display_help
            exit 1
            ;;
        *)
            optpdf "$i"
            exit 0
            ;;
    esac
done
