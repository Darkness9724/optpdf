CC = gcc
CCC = g++
CFLAGS = -O2 -pipe -Wall -Wextra -Werror
IFLAGS = -I/usr/include/ghostscript/ -I/usr/include/qpdf
LDFLAGS = -lgs -lqpdf
EXE = optpdf

default: build_c

build_c: optpdf.c
	$(CC) $(CFLAGS) $(IFLAGS) $^ $(LDFLAGS) -o $(EXE) 2>&1 | tee compile.log

build_cc: optpdf.cpp
	$(CCC) $(CFLAGS) $(IFLAGS) $^ $(LDFLAGS) -o $(EXE) 2>&1 | tee compile.log
	
dump: $(EXE)
	objdump -S -M intel -d $^ > obj.dump
	less obj.dump
	
analyze: optpdf.c optpdf.cpp
	cppcheck --enable=all --inconclusive --library=posix --check-config $^
	
memleak: $(EXE)
	valgrind -s --leak-check=full --show-leak-kinds=all ./$^

deps: $(EXE)
	@readelf -d $^ | grep NEEDED | awk '{ print $$6 }'
